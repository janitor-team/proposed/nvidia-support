# Translation of nvidia-support debconf messages to Portuguese
# Copyright (C) 2011 the nvidia-support's copyright holder
# This file is distributed under the same license as the nvidia-support package.
#
# Américo Monteiro <a_monteiro@netcabo.pt>, 2011.
# Miguel Figueiredo <elmig@debianpt.org>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: nvidia-support 20120630+2\n"
"Report-Msgid-Bugs-To: nvidia-support@packages.debian.org\n"
"POT-Creation-Date: 2012-09-07 18:52-0400\n"
"PO-Revision-Date: 2012-07-22 10:31+0100\n"
"Last-Translator: Miguel Figueiredo <elmig@debianpt.org>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Type: boolean
#. Description
#: ../nvidia-installer-cleanup.templates:2001
msgid "Run \"nvidia-installer --uninstall\"?"
msgstr "Executar \"nvidia-installer --uninstall\"?"

#. Type: boolean
#. Description
#: ../nvidia-installer-cleanup.templates:2001
msgid ""
"The nvidia-installer program was found on this system. This is probably left "
"over from an earlier installation of the non-free NVIDIA graphics driver, "
"installed using the NVIDIA *.run file directly. This installation is "
"incompatible with the Debian packages. To install the Debian packages "
"safely, it is therefore necessary to undo the changes performed by nvidia-"
"installer."
msgstr ""
"O programa nvidia-installer foi encontrado neste sistema. Provavelmente foi "
"deixado por uma instalação anterior do controlador gráfico, não-livre,  "
"NVIDIA instalado directamente com o ficheiro *.run da NVIDIA. Esta "
"instalação é incompatível com os pacotes Debian. Para instalar correctamente "
"os pacotes Debian é necessário desfazer as alterações executadas pelo nvidia-"
"installer."

#. Type: boolean
#. Description
#: ../nvidia-installer-cleanup.templates:3001
msgid "Delete nvidia-installer files?"
msgstr "Apagar os ficheiros do nvidia-installer?"

#. Type: boolean
#. Description
#: ../nvidia-installer-cleanup.templates:3001
msgid ""
"Some files from the nvidia-installer program still remain on this system. "
"These probably come from an earlier installation of the non-free NVIDIA "
"graphics driver using the *.run file directly. Running the uninstallation "
"procedure may have failed and left these behind. These files conflict with "
"the packages providing the non-free NVIDIA graphics driver and must be "
"removed before the package installation can continue."
msgstr ""
"Ainda permanecem no seu sistema alguns ficheiros do nvidia-installer. Estes "
"provavelmente vieram de uma instalação anterior do controlador gráfico não-"
"livre da NVIDIA usando directamente o ficheiro *.run da NVIDIA. Pode ter "
"falhado o procedimento de desinstalação e ter deixado estes ficheiros. Estes "
"ficheiros entram em conflito com os pacotes que disponibilizam o controlador "
"gráfico NVIDIA não-livre e têm de ser removidos antes da instalação do "
"pacote poder continuar."

#. Type: boolean
#. Description
#: ../nvidia-installer-cleanup.templates:4001
msgid "Remove conflicting library files?"
msgstr "Remover ficheiros de biblioteca em conflito?"

#. Type: boolean
#. Description
#: ../nvidia-installer-cleanup.templates:4001
msgid ""
"The following libraries were found on this system and conflict with the "
"current installation of the NVIDIA graphics drivers:"
msgstr ""
"As seguintes bibliotecas foram encontradas no seu sistema e estão em "
"conflito com a actual instalação do controlador gráfico da Nvida:"

#. Type: boolean
#. Description
#: ../nvidia-installer-cleanup.templates:4001
msgid ""
"These libraries are most likely remnants of an old installation using the "
"nvidia-installer program and do not belong to any package managed by dpkg. "
"It should be safe to delete them."
msgstr ""
"Estas bibliotecas são provavelmente restos de uma instalação antiga que "
"utilizou o programa nvidia-installer e não pertencem a nenhum pacote gerido "
"pelo dpkg. Deverá ser seguro apagá-las."

#. Type: error
#. Description
#: ../nvidia-support.templates:4001
msgid "Mismatching nvidia kernel module loaded"
msgstr "Módulo nvidia do kernel não coincidente carregado"

#. Type: error
#. Description
#: ../nvidia-support.templates:4001
msgid ""
"The NVIDIA driver that is being installed (version ${new-version}) does not "
"match the nvidia kernel module currently loaded (version ${running-version})."
msgstr ""
"O controlador NVIDIA que está a ser instalado (versão ${new-version}) não "
"coincide com o módulo nvidia do kernel actualmente carregado (versão "
"${running-version})."

#. Type: error
#. Description
#: ../nvidia-support.templates:4001
msgid "The X server, OpenGL, and GPGPU applications may not work properly."
msgstr ""
"O servidor X, OpenGL e aplicações GPGPU podem não funcionar correctamente."

#. Type: error
#. Description
#: ../nvidia-support.templates:4001
msgid ""
"The easiest way to fix this is to reboot the machine once the installation "
"has finished. You can also stop the X server (usually by stopping the login "
"manager, e.g. gdm3, sddm, or xdm), manually unload the module (\"modprobe -r nvidia"
"\"), and restart the X server."
msgstr ""
"A forma mais fácil de corrigir isto é reiniciar a máquina assim que a "
"instalação tiver terminado. Pode também parar o servidor X (normalmente "
"parando o gestor de logins, p.e. gdm3, sddm ou xdm), descarregar manualmente "
"o módulo (\"modprobe -r nvidia\") e reiniciar o servidor X."

#. Type: error
#. Description
#: ../nvidia-support.templates:5001
msgid "Conflicting nouveau kernel module loaded"
msgstr "Carregado o módulo do kernel noveau em conflito"

#. Type: error
#. Description
#: ../nvidia-support.templates:5001
msgid ""
"The free nouveau kernel module is currently loaded and conflicts with the "
"non-free nvidia kernel module."
msgstr ""
"O módulo livre de kernel nouveau está actualmente carregado e em conflito "
"com o módulo do kernel não-livre nvidia."

#. Type: error
#. Description
#: ../nvidia-support.templates:5001
msgid ""
"The easiest way to fix this is to reboot the machine once the installation "
"has finished."
msgstr ""
"A forma mais fácil de corrigir isto é reiniciar a máquina assim que a "
"instalação tiver terminado."

#. Type: note
#. Description
#: ../nvidia-support.templates:6001
msgid "Manual configuration required to enable NVIDIA driver"
msgstr "É necessária configuração manual para activar o controlador NVIDIA"

#. Type: note
#. Description
#: ../nvidia-support.templates:6001
msgid ""
"The NVIDIA driver is not yet configured; it needs to be enabled in xorg.conf "
"before it can be used."
msgstr ""
"O controlador NVIDIA ainda não está configurado; necessita ser activado no "
"xorg.conf antes de poder ser utilizado."

#. Type: note
#. Description
#: ../nvidia-support.templates:6001
msgid "Please see the package documentation for instructions."
msgstr "Para instruções por favor veja a documentação do pacote."

#. Type: error
#. Description
#: ../nvidia-support.templates:8001
msgid "NVIDIA driver is still enabled in xorg.conf"
msgstr "O controlador NVIDIA ainda está activo no xorg.conf"

#. Type: error
#. Description
#: ../nvidia-support.templates:8001
msgid ""
"The NVIDIA driver was just removed, but it is still enabled in the Xorg "
"configuration. X cannot be (re-)started successfully until NVIDIA is "
"disabled in the following config file(s):"
msgstr ""
"O controlador NVIDIA foi agora mesmo removido, mas ainda está activo na "
"configuração do Xorg. O X não pode ser (re)iniciado com sucesso até o NVIDIA "
"estar desabilitado no(s) seguinte(s) ficheiro(s) de configuração:"

#~ msgid ""
#~ "Note that switching to the free Nouveau driver requires the nvidia-kernel-"
#~ "common package to be purged (not just removed)."
#~ msgstr ""
#~ "Note que mudar para o controlador livre Nouveau necessita que o pacote "
#~ "nvidia-kernel-common seja purgado (e não apenas removido)."
